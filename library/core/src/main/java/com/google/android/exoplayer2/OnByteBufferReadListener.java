package com.google.android.exoplayer2;

import java.nio.ByteBuffer;

public interface OnByteBufferReadListener {
    void onByteBufferRead(final ByteBuffer byteBuffer);
}
